
/*
 * RRD4JChangeValues
 *
 * Permet de scipter des modifications dans les archives d'un fichier RRD4J.
 *
 * INSTALLATION :
 * Dans le même dossier mettre rrd4j-3.4.jar https://repo1.maven.org/maven2/org/rrd4j/rrd4j/3.4/
 * L'extraire
 * $ jar xf rrd4j-3.4.jar
 * Compilier
 * $ javac RRD4JChangeValues.java
 * Lancer
 * $ java RRD4JChangeValues
 *
 * LICENCE
 * gnu/gpl v3 https://www.gnu.org/licenses/gpl-3.0.html
 */


import org.rrd4j.core.*;
import java.io.IOException;

public class RRD4JChangeValues {

    private static String inputFilePath = "/home/jonas/rrd4j-demo/LinkyBase.rrd";
    public static void main(String[] args) {

        try {
            // open rrd
            RrdDb rrdDb = new RrdDb( inputFilePath );

            for ( int i=0; i<rrdDb.getDsCount(); i++ ) {
                Datasource ds = rrdDb.getDatasource(i);
                for ( int j = 0; j < rrdDb.getArcCount(); j++ ) {
                    Archive a = rrdDb.getArchive(j);
                    Long st = a.getStartTime();
                    Robin r = a.getRobin(i);
                    for ( int k = 0; k < r.getSize(); k++ ) {
                        double val = r.getValue(k);
                        if (!Double.isNaN(val) && (val > 100000.) ) {
                            System.out.println("Corrige " + st + ":" + val);
                            r.setValue(k, val/1000);
                        }
                        st += 60 * a.getSteps();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
